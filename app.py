#!/usr/bin/env python

from flask import Flask
from flask import render_template, make_response
from flask_restful import Resource, Api
from googlemaps.client import Client
gc = Client(key='AIzaSyAZvIw4moTSQkONKjt15pyOtbRqWzOkOlw')
import json, os

app = Flask(__name__)
api = Api(app)

class WherePlot(Resource):

    def get(self):
        places = self.process_json()
        return make_response(render_template('index.html', locs=places, data=self.data), 200)

    def process_json(self):
        with open('places.json', 'rb') as places:
            self.data = json.load(places)
            locs = [pl['city'] + ", " + pl['StateOrCountry']
                    for pl in self.data]
            geolocs = [gc.geocode(address=loc)[0]['geometry']['location']
                      for loc in locs]
            return geolocs

api.add_resource(WherePlot, '/')

port = os.getenv('VCAP_APP_PORT') or '8080'
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
