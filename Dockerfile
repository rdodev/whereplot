FROM python:2-slim

MAINTAINER "Ruben Orduz <ruben@7handles.io>"


ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "app.py"]


